﻿using System.IO;
using NUnit.Framework;

namespace tabs2spaces.Tests.Tabs2SpacesConverterTests
{
    [TestFixture]
    public class Convert
    {
        [Test]
        public void File1_content_given___correct_spaces_set()
        {
            string content  = TestFiles.File1;
            string expected = TestFiles.File1_expected;

            var sut = new Tabs2SpacesConverter();

            string actual = sut.Convert(content);
#if DEBUG
            File.WriteAllText("file1.txt"       , content);
            File.WriteAllText("file1-actual.txt", actual);
#endif
            Assert.AreEqual(expected, actual);
        }
    }
}