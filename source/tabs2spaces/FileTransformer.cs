﻿using System.IO;
using System.Text;

namespace tabs2spaces
{
    public class FileTransformer : IFileTransformer
    {
        private readonly byte[]               _bom                  = new byte[] { 0xef, 0xbb, 0xbf };
        private readonly Tabs2SpacesConverter _tabs2SpacesConverter = new Tabs2SpacesConverter();
        //---------------------------------------------------------------------
        public void Transform(string filePath)
        {
            string content = null;
            bool hasBOM    = false;

            using (FileStream fs = File.OpenRead(filePath))
            {
                hasBOM  = this.HasBom(fs);
                var sr  = new StreamReader(fs);
                content = sr.ReadToEnd();
            }

            content       = _tabs2SpacesConverter.Convert(content);
            Encoding utf8 = new UTF8Encoding(hasBOM);
            File.WriteAllText(filePath, content, utf8);
        }
        //---------------------------------------------------------------------
        private bool HasBom(FileStream file)
        {
            try
            {
                var tmp = new byte[3];

                if (file.Read(tmp, 0, 3) != tmp.Length) return false;

                return tmp[0] == _bom[0] && tmp[1] == _bom[1] && tmp[2] == _bom[2];
            }
            finally
            {
                file.Position = 0;
            }
        }
    }
}