﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace tabs2spaces
{
    internal class Runner
    {
        private readonly IFileTransformer _fileTransformer;
        //---------------------------------------------------------------------
        public Runner(IFileTransformer fileTransformer)
        {
            _fileTransformer = fileTransformer ?? throw new ArgumentNullException(nameof(fileTransformer));
        }
        //---------------------------------------------------------------------
        public void Run(string baseDirectory)
        {
            var extensions = new HashSet<string>(File.ReadLines("extensions.conf"));
            var files      = Directory
                .EnumerateFiles(baseDirectory, "*.*", SearchOption.AllDirectories)
#if !DEBUG
                .AsParallel()
#endif
                .Where(f => extensions.Contains(Path.GetExtension(f)));

#if DEBUG
            foreach (string file in files)
                _fileTransformer.Transform(file);
#else
            files.ForAll(_fileTransformer.Transform);
#endif
        }
    }
}