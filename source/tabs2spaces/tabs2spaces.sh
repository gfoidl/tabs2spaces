#!/bin/bash

help() {
	echo "converts tabs to spaces"
	echo "in all files configured in extensions.conf"
	echo ""
	echo "first argument is the base directory"
	echo "when not given, it is read from user interaction"
	
	exit 0
}

if [[ "$1" == "-h" || "$1" == "--help" ]]; then
	help
fi

exec dotnet tabs2spaces.dll
