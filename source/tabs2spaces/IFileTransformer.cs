﻿namespace tabs2spaces
{
    public interface IFileTransformer
    {
        void Transform(string filePath);
    }
}