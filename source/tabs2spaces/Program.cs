﻿using System;

namespace tabs2spaces
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string baseDirectory = null;

                if (args.Length == 1)
                    baseDirectory = args[0];
                else
                {
                    Console.Write("Base directory -> ");
                    baseDirectory = Console.ReadLine();
                }

                var runner = new Runner(new FileTransformer());
                runner.Run(baseDirectory);

                Console.WriteLine("done. bye");
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Error.WriteLine(ex.Message);
                Console.ResetColor();
            }
        }
    }
}