﻿using System;
using System.Text;

namespace tabs2spaces
{
    public class Tabs2SpacesConverter
    {
        private const int TabSize = 4;
        //---------------------------------------------------------------------
        public string Convert(string content)
        {
            char[] chars = content.ToCharArray();
            var sb       = new StringBuilder(content.Length);
            int curCol   = 1;

            for (int i = 0; i < chars.Length; ++i)
            {
                char c = chars[i];

                if (c == '\n')
                {
                    curCol = 1;

                    sb.Append(c);
                    continue;
                }

                if (c != '\t')
                {
                    sb.Append(c);
                    curCol++;
                    continue;
                }

                int tabs       = (int)Math.Ceiling((double)(curCol) / TabSize);
                int tabStopCol = tabs * TabSize;
                int spaces     = tabStopCol - curCol + 1;

                for (int j = 0; j < spaces; ++j)
                {
                    curCol++;
                    sb.Append(' ');
                }
            }

            return sb.ToString();
        }
    }
}