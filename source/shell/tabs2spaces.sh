#!/bin/bash

ext[0]=".cs"
ext[1]=".c"
ext[2]=".h"
ext[3]=".cpp"
ext[4]=".f95"
ext[5]=".f03"
ext[6]=".sql"
ext[7]=".ino"
ext[8]=".csproj"
ext[9]=".sqlproj"
ext[10]=".vcxproj"
ext[11]=".proj"
ext[12]=".targets"
ext[13]=".props"
ext[14]=".pubxml"
ext[15]=".config"
ext[16]=".settings"
ext[17]=".nuspec"
ext[18]=".nunit"
ext[19]=".cshtml"
ext[20]=".html"
ext[21]=".htm"
ext[22]=".ts"
ext[23]=".js"
ext[24]=".less"
ext[25]=".css"
ext[26]=".json"
ext[27]=".ashx"
ext[28]=".asax"
ext[29]=".svc"
ext[30]=".sh"
ext[31]=".bat"
ext[32]=".ps1"
ext[33]=".xml"
ext[34]=".xsd"
ext[35]=".txt"
ext[36]=".md"
ext[37]=".resx"
ext[38]=".tt"
ext[39]=".edmx"
ext[40]=".svg"
ext[41]=".xaml"
ext[42]=".dbml"
ext[43]=".tex"
ext[44]=".conf"

baseDir=

if [[ $# -lt 1 ]]; then
    read -p "Base directory -> " baseDir
else
    baseDir=$1
fi

echo "Converting files in $baseDir"

for ((i = 0; i < ${#ext[*]}; ++i)) do
    find . -name "*${ext[$i]}" -type f -exec bash -c 'expand -t 4 "$0" > /tmp/e && mv /tmp/e "$0"' {} \;
done
